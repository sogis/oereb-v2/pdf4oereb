package ch.so.agi.oereb.pdf4oereb;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import picocli.CommandLine.ArgGroup;

import java.util.concurrent.Callable;

import java.util.List;

@Command(
    name = "pdf4oereb", 
    mixinStandardHelpOptions = true,
    description = "Transforms a PLR DATA-Extract XML document into the corresponding PDF document."
)
public class App implements Callable<Integer> {

    @Option(names = "--input", paramLabel = "name", required = true, description = "Input DATA-Extract XML document.")
    String input = null;
    
    @Option(names = "--outputDir", paramLabel = "name", required = false, description = "Output directory where the files are stored. Default: ${DEFAULT-VALUE}")
    String outputDir = System.getProperty("java.io.tmpdir");
        
    @Option(names = "--pdf", required = false, description = "Create PDF file. Default: ${DEFAULT-VALUE}")
    boolean pdf = true;

    @Option(names = "--fo", required = false, description = "Create XSL-FO file.")
    boolean fo;

    public static void main(String[] args) {
        int exitCode = new CommandLine(new App()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Integer call() throws Exception {
//        if (xtf != null) {
//            OerebIconizer iconizer = new OerebIconizer();
//            List<LegendEntry> legendEntries =  iconizer.getSymbols("QGIS3", sldUrl, legendGraphicUrl);
//            iconizer.createXtf(legendEntries, xtf.fileName, xtf.basketId, xtf.typeCodeList, xtf.theme, xtf.subtheme);
//        } else {
//            OerebIconizer iconizer = new OerebIconizer();
//            List<LegendEntry> legendEntries = iconizer.getSymbols("QGIS3", sldUrl, legendGraphicUrl);
//            iconizer.saveSymbolsToDisk(legendEntries, downloadDir);
//        }
        
        return 0;
    }
}
